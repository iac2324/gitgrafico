import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;

public class Aplicacion {

	protected Shell shlAplicacin;
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Aplicacion window = new Aplicacion();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlAplicacin.open();
		shlAplicacin.layout();
		while (!shlAplicacin.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlAplicacin = new Shell();
		shlAplicacin.setBackground(SWTResourceManager.getColor(230, 97, 0));
		shlAplicacin.setSize(190, 136);
		shlAplicacin.setText("Aplicación");
		
		Label lblNewLabel = new Label(shlAplicacin, SWT.NONE);
		lblNewLabel.setBounds(10, 21, 63, 19);
		lblNewLabel.setText("Nombre:");
		
		text = new Text(shlAplicacin, SWT.BORDER);
		text.setBackground(SWTResourceManager.getColor(46, 194, 126));
		text.setBounds(79, 10, 83, 30);
		
		Button btnNewButton = new Button(shlAplicacin, SWT.NONE);
		btnNewButton.setBounds(68, 55, 83, 34);
		btnNewButton.setText("Cálculos");

	}
}
